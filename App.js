import React from 'react';
import { View, Image , StyleSheet, TextInput, KeyboardAvoidingView, Platform , Text, TouchableOpacity } from 'react-native';

var email = "";
var password = '';
var border='#766161';
function inputField(){
  if (email == ""){
    border = "'#cf0000'"
    console.log("border color is = "+border)
    console.log("email is = "+email)
    
  }
  else {
    border=="'#cf0000'"
    console.log("border color is = "+border)
    console.log("email is = "+email)
  }
  if (password == ''){
    border = "'#cf0000'"
    console.log("border color is = "+border)
    console.log("password is = "+password)
  }
  else {
    border== "'#cf0000'"
    console.log("border color is = "+border)
    console.log("password is = "+password)
  }
}
const Display = () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.titikatas}
        source={require('./Pattern.png')}
      />
      <Image
        style={styles.tinyLogo}
        source={require('./logo.png')}
      />
      <View>
        <Image 
        source={require('./email.png')}  
        style={styles.inputIcon}
        />
        <View style={styles.input}>
          <TextInput 
          placeholder="Username/Email" 
          onChangeText={ (text)=>email=text }/>
        </View>
      </View>
      <View>
        <Image 
          source={require('./password.png')}  
          style={styles.inputIcon}
          />
        <View style={styles.input}>
          <TextInput 
          placeholder="Password"
          onChangeText={ (text)=> password=text } />
        </View>
        <Image 
          source={require('./eyeoff.png')}  
          style={styles.eyeIcon}
          />
        </View>
      <TouchableOpacity style={styles.buttonContainer} onPress={() => inputField(this)}>
        <Text style={styles.buttonText} >Login</Text>
      </TouchableOpacity>
      <KeyboardAvoidingView
        style={styles.bottomBanner}
        behavior='padding'
        keyboardVerticalOffset={
          Platform.select({
            android: () => 0
          })()
        }
      >
          <View >
          <Image
            style={styles.bottomBanner}
            source={require('./bottomBanner.png')}
          />
          <Image
            style={styles.titikkotak}
            source={require('./titikkotak.png')}
          />
          <Text style= {styles.footerText}>Copyright 2021 | ATM Business Group</Text>
          </View>
          
      </KeyboardAvoidingView>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    paddingTop: 30,
    alignItems: 'center',
    // backgroundColor: '#a0937d',
    flex: 1,
  },
  tinyLogo: {
    marginTop: 50,
    width: 250,
    height: 50,
    marginBottom: 30,
  },
  input: {
    marginTop: 20,
    borderWidth: 1,
    borderColor: border,
    borderRadius: 10,
    width: 300,
    paddingLeft: 44,
    marginVertical: 10,
  },
  inputIcon: {
    position: 'absolute',
    top: 33,
    left: 10,
    width: 27,
    height: 27,
  },
  eyeIcon: {
    position: 'absolute',
    top: 33,
    right: 10,
    width: 27,
    height: 27,
  },
  
  bottomBanner: {
    height: 245,
    width: '100%',
    position: 'absolute',
    overflow: 'hidden',
    justifyContent: 'flex-end',
    bottom: 0,
    resizeMode: 'cover',
    // display: 'flex',
  },
  titikkotak: {
    bottom: 60,
  },
  titikatas: {
    left: 180,
    position: 'relative',
    marginTop: -40,
    elevation: 5,
    // backgroundColor: '#BCC8E7',
  },
  footerText:{
    fontSize: 15,
    color: 'white',
    left: 75,
    right: 75,
    bottom: 20,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer:{
    position: 'relative',
    left: 70,
    marginTop: 20,
    elevation: 5,
    backgroundColor: "#DC241F",
    borderRadius: 10,
    paddingVertical: 15,
    paddingHorizontal: 25
  },
  buttonText: {
    fontSize: 13,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
  }
});
export default Display;